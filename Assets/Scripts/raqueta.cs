﻿using UnityEngine;
using System.Collections;

public class raqueta : MonoBehaviour {
	[SerializeField] private float speed = 30;
	[SerializeField] private string axis = "Vertical";
	// Use this for initialization
	void Start () {
	
	}
	void FixedUpdate ()
	{
		//Detect Keyboard for any player.


			float v = Input.GetAxisRaw(axis);
			GetComponent<Rigidbody2D>().velocity = new Vector2(0, v) * speed;


	
	}
	// Update is called once per frame
	void Update () {
	
	}
}
